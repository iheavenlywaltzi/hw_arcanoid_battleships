#include "inc.h"

int main( int argc, char *argv[] ) {
//Start up SDL and create window
	if( init() ){
		//Load media
		if( loadMedia() ) {	
			bool quit = true; //Main loop flag
			SDL_Event e; //Event handler
			
			int frameWater = 0; // итератор
			int frameW = 0; //
			int textA = 100; // итератор для анимации текста
			bool textAFlagRevers = false; // флажочек для обратной анимации текста
			bool textView = true; // флажочек для показа текста
			bool nextLvl = false; // Переключатель на следующий уровень
			
			//Set text color as black
			SDL_Color textColor = { 0, 0, 0, 255 };
			SDL_Color textColorBig = { 255, 255, 255, 1 };

			Ships.loadTexture(TSShips);// загрузка текстуры коробля
			cannonBall.loadTexture(TSCannonBall); // Загрузка текстуры ядра

			//int timeIterStart = 0;
			Mix_PlayMusic(backgroundMusic, -1 );
			while( quit ) {
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 ) {
					//User requests quit
					if( e.type == SDL_QUIT ){ quit = false; }
					
					Ships.eventCheck(e);
					cannonBall.eventCheck(e);
					
					if( e.type == SDL_KEYDOWN && e.key.repeat == 0 && e.key.keysym.sym == SDLK_SPACE && nextLvl) {
						nextLvl = false;
						textA = 10;
						textAFlagRevers = false;
						Mix_PlayMusic(backgroundMusic, -1 );
					}
					if( e.type == SDL_KEYDOWN && e.key.repeat == 0 && e.key.keysym.sym == SDLK_SPACE && textView) {
						textView = false;
						textA = 10;
						textAFlagRevers = false;
					}
					
					if( e.type == SDL_KEYDOWN && e.key.repeat == 0 && e.key.keysym.sym == SDLK_r && G_pause) {
						Mix_PlayMusic(backgroundMusic, -1 );
						cannonBall.restart();
						Chests.clear();
						Chests.init(G_StartLineChest,G_StartSumLineChest); // подготовим всё для отрисовки кучи ящиков
						G_box_view = 0;
						G_lvl = 1;
						G_speedShips = 2; 
						G_speedCannonball = 2;
						
						G_pause = false;
					}
					
					if( e.type == SDL_KEYDOWN && e.key.repeat == 0 && e.key.keysym.sym == SDLK_r && G_pause) {
						
					}
				}
				
				if (!G_pause){
					// Движение
					Ships.move();
					cannonBall.move();
					
					//Clear screen
					SDL_SetRenderDrawColor( gRenderer, 80, 80, 80, 0xFF );
					SDL_RenderClear( gRenderer );
					TSBack.render( 0, Ships.getPosX()/100 ); // рисуем фон
					TSBackLife.render( 1-(Ships.getPosX()/10+5), 388 ); // рисуем фон
					
					//рисуем ящики и тамже колизия проверяется
					Chests.render();	

					//Set text to be rendered
					timeText.str( "" );
					timeText << "BOX:" << G_box_view; 
					if( !gFPSTextTexture.loadFromRenderedText( gFont, timeText.str().c_str(), textColor ) ) { printf( "Unable to render texture!\n" );}
					gFPSTextTexture.render( 710, 15 ); //Render textures
					
					timeText.str( "" );
					timeText << "LVL:" << G_lvl; 
					if( !gFPSTextTexture.loadFromRenderedText( gFont, timeText.str().c_str(), textColor ) ) { printf( "Unable to render texture!\n" );}
					gFPSTextTexture.render( 20, 15 ); //Render textures	
					
					if (textView){
						textColorBig.a = textA;
						printBigText(250, 280, "SPACE - Start game", textColorBig);
						printBigText(250, 310, "A - Left move", textColorBig);
						printBigText(250, 340, "D - Right move", textColorBig);
						printBigText(250, 370, "R - Restart game", textColorBig);
						
						(textA >= 10 && textAFlagRevers) ? --textA : textAFlagRevers = false;
						if (textA <= 150 && !textAFlagRevers) {++textA;}
						else {textAFlagRevers = true;}
					}
					
					if (nextLvl){
						textColorBig.a = textA;
						printBigText(250, 280, "Level passed !!!", textColorBig);
						printBigText(250, 310, "SPACE - Next LvL", textColorBig);
						
						(textA >= 10 && textAFlagRevers) ? --textA : textAFlagRevers = false;
						if (textA <= 150 && !textAFlagRevers) {++textA;}
						else {textAFlagRevers = true;}
					}
					
					cannonBall.render(); //Анимация, движение шарика
					
					SDL_Rect currentClipWater = {0,40*frameW,SCREEN_WIDTH,40};
					TSWater.render( 0, 554, &currentClipWater );

					Ships.render(); //Анимация, движение коробля
					
					SDL_Rect currentClipWater2 = {0,40*( 3 -frameW),SCREEN_WIDTH,40};
					TSWater.render( 0, 576, &currentClipWater2 );

					//Update screen
					SDL_RenderPresent( gRenderer );
					
					// разные расчёты
					frameWater++;
					frameW = frameWater / 20;
					if(frameW >= 4) {frameWater = 0; frameW = 0;}
					if (Chests.getSumChest() <= 0){ 
						nexе_lvl();
						nextLvl = true;
					}
				} else {
					textColorBig.a = 180;
					printBigText(250, 280, "GAME OVER", textColorBig);
					printBigText(250, 310, "R - Restart game ", textColorBig);

					//Update screen
					SDL_RenderPresent( gRenderer );
				}
				
				SDL_Delay(SCREEN_DELAY);
			}
		}
	}

	//Free resources and close SDL
	close();

	//system("pause");
	return 0;
}