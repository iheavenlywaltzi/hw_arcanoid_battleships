#pragma once
#ifndef _INC_H_
#define _INC_H_

#include <SDL2\SDL.h>
#include <SDL2\SDL_image.h>
#include <SDL2\SDL_ttf.h>
#include <SDL2\SDL_mixer.h>
#include <string>
#include <iostream>
#include <sstream>

#include "core/prototypes/classes.h"
#include "core/prototypes/functions.h"
#include "core/game_settings.h"
#include "core/variables.h"

#endif