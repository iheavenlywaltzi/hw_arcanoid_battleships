#pragma once
#ifndef _GAME_SETTINGS_H_
#define _GAME_SETTINGS_H_

extern const int SCREEN_DELAY = 16; // Задержка между кадрами в милисек
extern const int SCREEN_WIDTH = 800; // ширина экрана
extern const int SCREEN_HEIGHT = 600; // высота экрана
extern const int Y_POS_SHIPS = 480; // Положение кораблика по высоте 

int G_lvl = 1; // Текущий уровень игры
int G_StartLineChest = 1; // Линии сундуков ( начальные )
int G_StartSumLineChest = 4; // количество сундуков в линии ( начальные )
int G_speedShips = 3; // скорость движения коробля ( начальные )
int G_speedCannonball = 2; // скорость полёта ядер ( начальные )

int G_maxLine = 4; // Максимум Линий сундуков
int G_maxChest = 12; // Максимум сундуков в 1й линии
int G_startLineX = 50; // Начало первой линии по  X 
int G_startLineY = 50; // Начало первой линии по  Y

int G_speedAnimShips = 10; // Скорость анимации при плавании коробля
int G_box_view = 0; // кол-во уничтоженных боксов ( началное )
bool G_pause = false; // играем или нет

#endif