#pragma once
// различные Глобальные переменные для работы
SDL_Window* gWindow = NULL; //The window we'll be rendering to
SDL_Renderer* gRenderer = NULL; //The window renderer

//Шрифт
TTF_Font* gFont = NULL;
TTF_Font* gFontBig = NULL;

//Текстуры
LTexture TSWater; // Вода
LTexture TSBack; // бек граунд
LTexture TSBackLife; // бек Движущейся
LTexture TSShips; // кораблик
LTexture TSСhest; // Сундуки
LTexture TSCannonBall; // пушачное ядро
LTexture gFPSTextTexture; // текст

//Музыка
Mix_Music *backgroundMusic = NULL;
Mix_Music *endGame = NULL;
Mix_Music *lvlUp = NULL;

//Семплы
Mix_Chunk *canonShot = NULL;
Mix_Chunk *hit = NULL;
Mix_Chunk *hitShip = NULL;
Mix_Chunk *splashWater = NULL;
Mix_Chunk *chestEnd = NULL;

// Разное
controller Ships; // Запуск класса корабля
chest Chests; // Класс сундуков
ball cannonBall; // Запуск пушечного ядра

//In memory text stream
std::stringstream timeText;