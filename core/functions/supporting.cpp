#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>
#include <SDL2\SDL_mixer.h>

#include <iostream>
#include <string>
#include <sstream>

#include "../prototypes/classes.h"

// variable
extern int G_lvl;
extern int G_StartLineChest;
extern int G_StartSumLineChest;
extern int G_speedShips;
extern int G_speedCannonball;

// fcn
extern int random(int low, int high);
extern void printBigText(int x, int y, std::string str, SDL_Color &color);
extern void nexе_lvl();

// class
extern std::stringstream timeText;
extern LTexture gFPSTextTexture;
extern TTF_Font* gFontBig;
extern ball cannonBall;
extern chest Chests;
extern Mix_Music *lvlUp;


//да да кудаже без неё функции рандома то в играх....
int random(int low, int high) {return low + rand() % (high - low + 1);} 

// достало по 3 раза одно итоже писать
void printBigText(int x, int y, std::string str, SDL_Color &color){
	timeText.str( "" );
	timeText << str; 
	if( !gFPSTextTexture.loadFromRenderedText( gFontBig, timeText.str().c_str(), color ) ) { printf( "Unable to render texture!\n" );}
	gFPSTextTexture.render( x, y ); //Render textures
}

// Увеличение уровня игры
void nexе_lvl() { 
	++G_lvl;
	G_speedShips = G_lvl+1; // скорость движения коробля ( начальные )
	G_speedCannonball = G_lvl; // скорость полёта ядер ( начальные )
	cannonBall.restart();
	Chests.clear();
	Chests.init((G_StartLineChest+G_lvl-1),G_StartSumLineChest+G_lvl); // подготовим всё для отрисовки кучи ящиков
	Mix_PlayMusic(lvlUp, 1 );
}