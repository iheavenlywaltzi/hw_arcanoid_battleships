#include <SDL2\SDL.h>
#include <SDL2\SDL_image.h>
#include <SDL2\SDL_ttf.h>
#include <SDL2\SDL_mixer.h>

#include <iostream>

#include "../prototypes/classes.h"

// fcn
extern bool init();

// различные Глобальные переменные для работы
extern const int SCREEN_WIDTH; // ширина экрана
extern const int SCREEN_HEIGHT; // высота экрана

extern int G_StartLineChest; // Линии сундуков ( начальные )
extern int G_StartSumLineChest; // количество сундуков в линии ( начальные )

extern SDL_Window* gWindow; //The window we'll be rendering to
extern SDL_Renderer* gRenderer; //The window renderer
extern chest Chests; // Класс сундуков
extern ball cannonBall; // Класс сундуков

// запускаем,заполняем,создаём  делаем всё чтобы игра начала работать инициализируя нужные данные.
bool init() {
	srand(time(NULL));

	Chests.init(G_StartLineChest,G_StartSumLineChest); // подготовим всё для отрисовки кучи ящиков
	Chests.colision(&cannonBall); // передадим объект для проверки колизии

	bool success = true; //Initialization flag
	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() ); success = false;}
	else {
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ){printf( "Warning: Linear texture filtering not enabled!" );}

		//Create window
		gWindow = SDL_CreateWindow( "Ships Battle", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL ) { printf( "Window created Error: %s\n", SDL_GetError() ); success = false; }
		else {
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL ) { printf( "Renderer Error: %s\n", SDL_GetError() ); success = false; }
			else {
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) ) { printf( "SDL_image init Error: %s\n", IMG_GetError() ); success = false; }
				
				//Initialize SDL_ttf
				if( TTF_Init() == -1 ){	printf( "SDL_ttf initialize! Error: %s\n", TTF_GetError() ); success = false; }
				
				//Initialize SDL_mixer
				if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 ) { printf( "SDL_mixer initialize! Error: %s\n", Mix_GetError() );success = false;}
			}
		}
	}
	return success;
}