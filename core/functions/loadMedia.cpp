#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>
#include <SDL2\SDL_mixer.h>

#include <iostream>

#include "../prototypes/classes.h"

// fcn
extern bool loadMedia();

//Шрифт
extern TTF_Font* gFont;
extern TTF_Font* gFontBig;

//Текстуры
extern LTexture TSWater; // Вода
extern LTexture TSBack; // бек граунд
extern LTexture TSBackLife; // бек Движущейся
extern LTexture TSShips; // кораблик
extern LTexture TSСhest; // Сундуки
extern LTexture TSCannonBall; // пушачное ядро
extern LTexture gFPSTextTexture; // текст

//Музыка
extern Mix_Music *backgroundMusic;
extern Mix_Music *endGame;
extern Mix_Music *lvlUp;

//Семплы
extern Mix_Chunk *canonShot;
extern Mix_Chunk *hit;
extern Mix_Chunk *hitShip;
extern Mix_Chunk *splashWater;
extern Mix_Chunk *chestEnd;

// Загрузим Все медиа файлы то необходимы
bool loadMedia() {
	bool success = true; //Loading success flag

	//Load sprite texture
	if( !TSBack.loadFromFile( "data/img/background.png" ) ) {printf( "Failed to load Cannon_Ball!\n" );success = false;}
	if( !TSBackLife.loadFromFile( "data/img/live_background.png" ) ) {printf( "Failed to load Cannon_Ball!\n" );success = false;}
	if( !TSCannonBall.loadFromFile( "data/img/Cannon_Ball_mini.png" ) ) {printf( "Failed to load Cannon_Ball!\n" );success = false;}
	if( !TSShips.loadFromFile( "data/img/shipsheet.png" ) ) {printf( "Failed to load shipsheet!\n" );success = false;}
	if( !TSСhest.loadFromFile( "data/img/chest_sprite.png" ) ) {printf( "Failed to load shipsheet!\n" );success = false;}
	if( !TSWater.loadFromFile( "data/img/water.png" ) ) {printf( "Failed to load shipsheet!\n" );success = false;}
	
	//Open the font
	gFont = TTF_OpenFont( "data/fonts/VT323-Regular.ttf", 24 );
	if( gFont == NULL )	{ printf( "Failed font! SDL_ttf Error: %s\n", TTF_GetError() ); success = false; }	
	
	gFontBig = TTF_OpenFont( "data/fonts/VT323-Regular.ttf", 32 );
	if( gFontBig == NULL )	{ printf( "Failed font! SDL_ttf Error: %s\n", TTF_GetError() ); success = false; }
	
	//Load music
    backgroundMusic = Mix_LoadMUS( "data/sounds/back_more.wav" );
    if( backgroundMusic == NULL ) {printf( "Failed load music! SDL_mixer Error: %s\n", Mix_GetError() );  success = false;  }
	
    endGame = Mix_LoadMUS( "data/sounds/end_game.wav" );
    if( endGame == NULL ) {printf( "Failed load music! SDL_mixer Error: %s\n", Mix_GetError() );  success = false;  }    
	
	lvlUp = Mix_LoadMUS( "data/sounds/lvl_up.wav" );
    if( endGame == NULL ) {printf( "Failed load music! SDL_mixer Error: %s\n", Mix_GetError() );  success = false;  }
    
    //Load sound effects
    canonShot = Mix_LoadWAV( "data/sounds/explode.wav" );
    if( canonShot == NULL ) {printf( "Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError() ); success = false;}  
	
    hit = Mix_LoadWAV( "data/sounds/bump.wav" );
    if( hit == NULL ) {printf( "Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError() ); success = false;}  
	
    hitShip = Mix_LoadWAV( "data/sounds/pew.wav" );
    if( hitShip == NULL ) {printf( "Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError() ); success = false;} 	
    
	splashWater = Mix_LoadWAV( "data/sounds/splash_water.wav" );
    if( splashWater == NULL ) {printf( "Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError() ); success = false;} 
    
	chestEnd = Mix_LoadWAV( "data/sounds/chest_del.wav" );
    if( chestEnd == NULL ) {printf( "Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError() ); success = false;} 

	return success;
}